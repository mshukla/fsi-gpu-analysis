from pyfsi_gpu.fsiEnums import *

# TLM-8700 laser init parameters
class LaserInitParams:
    PORT        = 'COM4'
    INTERLOCK   = LASER_INTERLOCK.UNLOCK        # Laser interlock state
    STATE       = LASER_STATE.ON                # Turn laser on
    UNIT        = LASER_PWR_UNIT.MW             # Laser power units mW or dBm
    POWER       = 1.0                           # Laser power
    DOMAIN      = LASER_DOMAIN.WAVELENGTH       # Wavelength or frequency
    MOD_SOURCE  = LASER_MOD_SOURCE.NO_MOD       # Laser modulation source
    START       = 1520                          # Sweep start wavelength
    STOP        = 1570                          # Sweep stop wavelength
    SCAN_MODE   = LASER_SCAN_MODE.UNI_FORWARD   # Sweep mode
    SCAN_SPEED  = 2000                          # Sweep speed
    CYCLES      = 1                             # Number of sweep cycles
    TRIGGER_POL = TRIG_POLARITY.ACTIVE_HIGH     # Trigger and SYNC polarity

# PicoScope init parameters
class PicoScopeInitParams:
    PICO_MODEL               = '4824'                              # PicoScope model used, '4824' or '3403D' 
    PICO_SAMPLING_RATE       = SAMPLING_RATE_PS4000.R80MSPS        # Acquisition sampling rate
    PICO_NUMBER_OF_SAMPLES   = 1                                   # Calculate the number of acquisition samples per channel based on laser and pico settings
    PICO_TRIGGER_CH          = PICO_CHANNEL.A                      # Channel on which the trigger signal will appear
    PICO_TRIGGER_CH_RANGE    = PICO_RANGE.R2V
    PICO_TRIGGER_ENABLE      = 1                                   # Enable trigger: 0 - disabled; 1 - enabled
    PICO_TRIGGER_THRESHOLD   = 1024                                # Trigger threshold value in bits!
    PICO_TRIGGER_EDGE        = TRIGGER_EDGE.RISING                 # Trigger polarity
    PICO_TRIGGER_DELAY       = 0                                   # Delay after the trigger signal occurs
    PICO_TRIGGER_TIMEOUT     = 0                                   # Timeout after witch the acquisition is triggered if no trigger signal appears; 0 - wait forever
    PICO_MEAS_CH             = PICO_CHANNEL.G                      # PicoScope channel connected to the measurement interferometer output                         
    PICO_REF_CH              = PICO_CHANNEL.C                      # PicoScope channel connected to the reference interferometer output
    PICO_REF_CH_RANGE        = PICO_RANGE.R500MV                   # Reference interferometer channel range
    PICO_GAS_CELL_CH         = PICO_CHANNEL.B                      # PicoScope channel connected to the gas cell output
    PICO_GAS_CELL_CH_RANGE   = PICO_RANGE.R500MV                   # Gas cell channel range

# EDFA amplifier init parameters
class EdfaInitParams:
    PORT    = 'Not used'    # EDFA com port       
    POWER   = '10.0'        # EDFA output power (dBm)
    STATE   = 'Off'         # EDFA state - On/Off

# Fiber optic switch init parameters
class FoSwitchInitParams:
    PORT    = 'COM5'       # Fiber optic switch com port

# Photodetection module init parameters
class PhotoModuleInitParams:
    PORT            = 'Not used'    # Photodetection module com port, if 'Not used' - Thorlabs photodiodes
    VERSION         = 'Thorlabs'    # Photodetection module version
    MEAS_GAIN       = '10k'         # Photodetection module gain on measurement channel
    GAS_CELL_GAIN   = '10k'         # Photodetection module gain on gas cell channel
    REF_INT_GAIN    = '10k'         # Photodetection module gain on reference interferometer channel

class ComputationConfig:
    ZPF = 3 # Zero Padding Factor for FFT computation
