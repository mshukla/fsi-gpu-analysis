#!/usr/bin/env
# vim: ts=8 sts=4 sw=4 et ai

import os
import datetime as dt
import re
import glob

pattern = re.compile(
    r'fsi_acq_((\d+)-(\d+)-(\d+)-(\d+)h(\d+)m(\d+)s)-b7-ch(\d).dat')

def get_files(basedir, timestamp):
    chs = []
    paths = []
    flist = glob.glob(os.path.join(basedir, '*' + timestamp + '*.dat'))
    for f in flist:
        fname = os.path.basename(f)
        ts, year, month, day, hour, minute, second, ch = pattern.match(fname).groups()
        chs.append(ch)
        paths.append(f)
    return chs, paths

def check_data(path, time_req):

    channel_id=[]
    path_list=[]
    today = dt.datetime.now().date()
    for fname in os.listdir(path):
        file_path = os.path.join(path, fname)
        if os.path.isfile(file_path):
            file_extension = os.path.splitext(file_path)[1]
        if (file_extension.lower() == ".dat"):
            ts, year, month, day, hour, minute, second, ch = pattern.match(fname).groups()
            channel_id.append(ch)
            path_list.append(file_path)

    print(f'channel_id = {channel_id}, path_list = {path_list}')
    return channel_id, path_list


def channel_type(ref_ch, gas_ch, meas_ch, path, time_req):
    channels=[]
    path_list=[]

    channels, path_list = get_files(path, time_req)
    ref_path = path_list[channels.index(str(ref_ch))]
    gas_path = path_list[channels.index(str(gas_ch))]
    meas_path = path_list[channels.index(str(meas_ch))]

    return ref_path, gas_path, meas_path
