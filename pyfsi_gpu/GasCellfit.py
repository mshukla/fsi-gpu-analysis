#!/usr/bin/env python
# coding: utf-8

import cupy as cp
import numpy as np
import matplotlib.pyplot as plt

def detect_peaks_gpu(x, mph=None, mpd=1, threshold=0, edge='rising',
                 kpsh=False, valley=False, show=False, ax=None, title=True):

    """
	Based on: https://nbviewer.org/github/demotu/BMC/blob/master/notebooks/DetectPeaks.ipynb
        And changed using cupy library
    """
    x = cp.atleast_1d(x).astype('float64')
    if x.size < 3:
        return cp.array([], dtype=int)
    if valley:
        x = -x
        if mph is not None:
            mph = -mph
    # find indices of all peaks
    dx = x[1:] - x[:-1]
    # handle NaN's
    indnan = cp.where(cp.isnan(x))[0]
    if indnan.size:
        x[indnan] = np.inf
        dx[cp.where(cp.isnan(dx))[0]] = np.inf
    ine, ire, ife = cp.array([[], [], []], dtype=int)
    if not edge:
        ine = cp.where((cp.hstack((dx, 0)) < 0) & (cp.hstack((0, dx)) > 0))[0]
    else:
        if edge.lower() in ['rising', 'both']:
            ire = cp.where((cp.hstack((dx, 0)) <= 0) & (cp.hstack((0, dx)) > 0))[0]
        if edge.lower() in ['falling', 'both']:
            ife = cp.where((cp.hstack((dx, 0)) < 0) & (cp.hstack((0, dx)) >= 0))[0]
    ind = cp.unique(cp.hstack((ine, ire, ife)))
    # handle NaN's
    if ind.size and indnan.size:
        # NaN's and values close to NaN's cannot be peaks
        ind = ind[cp.in1d(ind, cp.unique(cp.hstack((indnan, indnan-1, indnan+1))), invert=True)]
    # first and last values of x cannot be peaks
    if ind.size and ind[0] == 0:
        ind = ind[1:]
    if ind.size and ind[-1] == x.size-1:
        ind = ind[:-1]
    # remove peaks < minimum peak height
    if ind.size and mph is not None:
        ind = ind[x[ind] >= mph]
    # remove peaks - neighbors < threshold
    if ind.size and threshold > 0:
        dx = cp.min(cp.vstack([x[ind]-x[ind-1], x[ind]-x[ind+1]]), axis=0)
        ind = np.delete(cp.asnumpy(ind, cp.where(dx < threshold)[0]))
    # detect small peaks closer than minimum peak distance
    if ind.size and mpd > 1:
        ind = ind[cp.argsort(x[ind])][::-1]  # sort ind by peak height
        idel = cp.zeros(ind.size, dtype=bool)
        for i in range(ind.size):
            if not idel[i]:
                # keep peaks with the same height if kpsh is True
                idel = idel | (ind >= ind[i] - mpd) & (ind <= ind[i] + mpd)  & (x[ind[i]] > x[ind] if kpsh else True)
                idel[i] = 0  # Keep current peak
        # remove the small peaks and sort back the indices by their occurrence
        ind = cp.sort(ind[~idel])
    
    return ind


def gascell_alpha(gascellData, wavelengthListPath, time_period, plot=False):
    """
     Keyword arguments:
    ------------------
    GasCellData -- filtered gas cell signal
    wavelengthListPath  -- path to the NIST data for the HCN cell (SRM2519a.csv)
    plot                -- Boolean to display all plots for debugging
    Return:
    -----------
    sweep rate 
    
    """
    maxGasCellData = cp.max(gascellData)
    minGasCellData = cp.min(gascellData)
    signalTreshold = (minGasCellData + 0.70*(cp.abs(maxGasCellData) - cp.abs(minGasCellData)))
    ind = detect_peaks_gpu(-(gascellData),mph=-signalTreshold,mpd=3000,threshold=-signalTreshold, show=False)
 
    ind2 =cp.asnumpy(ind)   

    gascellData=cp.asnumpy(gascellData)

    if plot:
        plt.plot(-gascellData)
        plt.plot(ind2,-gascellData[ind2] ,'x')
        plt.show()
    gascellData=cp.array(gascellData) 
    t_sample = time_period
    cLight      = 299792458 
    gas_cell_nist= np.genfromtxt(wavelengthListPath)
    gas_cell_nist = cLight/(gas_cell_nist * 1e-9)
    
    gc_idxs=ind
    gas_cell_meas= gc_idxs*t_sample
    
    gas_cell_nist = cp.array(gas_cell_nist)
    gas_cell_meas = cp.array(gas_cell_meas)
    
    peak_pos_meas_diff=abs(cp.diff(cp.diff(gas_cell_meas)))
    peak_pos_nist_diff=abs(cp.diff(cp.diff(gas_cell_nist)))

    peak_pos_meas=cp.argwhere(peak_pos_meas_diff > cp.mean(peak_pos_meas_diff))
    peak_pos_nist=cp.argwhere(peak_pos_nist_diff > cp.mean(peak_pos_nist_diff))

    print(len(peak_pos_meas))
    print(len(peak_pos_nist))
    peak_pos_meas=peak_pos_meas[0]+2
    peak_pos_nist=peak_pos_nist[0]+2

    
    g0 = gas_cell_meas[peak_pos_meas]
    n0 = gas_cell_nist[peak_pos_nist]


    l = min(peak_pos_nist, peak_pos_meas)
    r = min(len(gas_cell_nist) - peak_pos_nist, len(gas_cell_meas) - peak_pos_meas)    

    p0= (peak_pos_nist-l).item()
    pend= (peak_pos_nist+r).item()

    gstart= (peak_pos_meas-l).item()
    gend= (peak_pos_meas+r).item()
    
    nn = gas_cell_nist[p0:pend]
    xx = gas_cell_meas[gstart:gend]
    peak_idx = l 
   
    xx = xx - g0
    nn = nn - n0

    
    alpha = cp.dot(xx,nn)/cp.dot(xx, xx)
    err = cp.linalg.norm(alpha * xx - nn)
    sweep= n0 + alpha*(xx)
    #########alpha=cLight/(alpha)    
    print("alpha",alpha)
    return alpha
