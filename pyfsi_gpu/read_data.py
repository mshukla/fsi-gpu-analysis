
import cupy as cp
import csv
import numpy as np
from collections import defaultdict

def read_file(path):
    board, channel, Sample, Value= [],[],[],[]


    with open(path, "r") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter='|')
        next(csv_reader)
        for lines in csv_reader:
            board.append(int(lines[0],16))
            channel.append(int(lines[1],16))
            Sample.append(int(lines[2],16))
            Value.append(int(lines[3],16))
        
    board=np.array(board)
    board=board[1:]
    channel=channel[1:]
    Sample=np.array(Sample)
    Sample=Sample[1:]
    Value=np.array(Value[1:])
    Value=Value[1:]
    channel_used=list(set(channel))


    data=defaultdict(list)

    for chan in channel_used:
        for i in range(0,channel.count(chan)):
            data[chan].append(Value[i])

    meas=data[channel_used[0]]
    ref=data[channel_used[1]]
    gas=data[channel_used[2]]



    meas=cp.array(meas)
    ref=cp.array(ref)
    gas=cp.array(gas)

    return ref,gas,meas




def read_ref(path):
    
    ref = np.loadtxt(path, unpack=True, skiprows=5)
    return ref


def read_gas(path):
    
    gas = np.loadtxt(path, unpack=True, skiprows=5)
    return gas

def read_meas(path):
    
    meas = np.loadtxt(path, unpack=True, skiprows=5)
    return meas  
