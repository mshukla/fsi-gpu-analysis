#!/usr/bin/env python
# coding: utf-8

import scipy.io as sio
import numpy
import scipy
import numpy as np 
from pyfsi_gpu.fsiEnums import * 
from pyfsi_gpu.fsiExceptions import *
import math
from lmfit.models import LorentzianModel, GaussianModel 
import logging
import cupy as cp
import cupyx.scipy.linalg
import matplotlib.pyplot as plt



def dataCutout(distance, power, minDist, maxDist):
    """
    Cut out the distance region of interest from the input data.

    Keyword arguments:
    ------------------
    distance    -- array with the distance data (from FFT)
    power       -- array with the magnitude data (from FFT)
    minDist     -- minimum distance boundary
    maxDist     -- maximum distance boundary

    Return:
    -------
    distance[]  -- array containing the cutout distance values
    power[]     -- array containing the magnitude values coresponding to the distance values
    """
    minIdx = -1
    maxIdx = -1
    for i in range(0, len(distance)):
        if minIdx == -1:
            if distance[i] >= minDist:
                minIdx = i

        if maxIdx == -1:
            if distance[i] >= maxDist:
                maxIdx = i
    
    indexesToCutOut = np.arange(int(minIdx), int(maxIdx))

    return distance[indexesToCutOut], power[indexesToCutOut]



def fftFitValid(fitResult):
    '''
    Validates whether the fit result is within the acceptable error range
    
    Arguments:
    ----------
    fitResult - output of the lmfit fitting function

    Return:
    -------
    True if ok, False if error is too big
    '''
    ret = True

    if(isinstance(fitResult.params['center'].stderr, (int, float)) and isinstance(fitResult.params['amplitude'].stderr, (int, float))):

        fftFitCenterErrorThreshold = 20e-5     # Error on the frequency (distance) value in m
        fftFitAmplitudeErrorThreshold = 100   # Error on the amplitude of the peak in % - determined emiprically

        if (fitResult.params['center'].stderr > fftFitCenterErrorThreshold): # Frequency (distance)
            ret = False
            print('Center error: ' + str(fitResult.params['center'].stderr))

        if ((fitResult.params['amplitude'].stderr/fitResult.params['amplitude'].value)*100 > fftFitAmplitudeErrorThreshold): # Amplitude
            ret = False
            print('Amplitude error: ' + str(fitResult.params['amplitude'].stderr))

        print(str((fitResult.params['center'].stderr/fitResult.params['center'].value)*100))
        print(str((fitResult.params['amplitude'].stderr/fitResult.params['amplitude'].value)*100))

    return ret


def fitFsiPeaks(distanceData, powerData, fsiParamsHandle, opMode, fullOutput=False, fit=False):
    """
    Function which fits the interferometric peaks.
    Iterates over the currentDistences given in the fsiParams of the current channel.
    
    Keyword arguments:
    ------------------
    distanceData    -- array with the distance values (from FFT)
    powerData       -- array with the magnitude data (from FFT)
    fsiParamsHandle -- parameters of the measurement channel, FsiSignalParams-type
    opMode          -- setup operating mode SETUP_OP_MODE-type

    Return:
    -------
    if fullOutput == False: Peak center values numpy array-type. Number of elements depends on the number of currentDistance values. Returns '0.0' in case the fitting function fails.
    if fullOutput == True: Outputs raw fit data
    warningMsg - if applicable
    """
    returnValues = np.array([])
    retStatus = {'Message':'OK', 'Problem':'', 'Center error': np.array([])} # Returned status message
    fitMagnitudeData = {}
    fitDistanceData = {}

    # If there are no initial values specified...
    if(fsiParamsHandle.currentDistance == []):
            retStatus['Message'] = 'No initial values specified for CH: ' + str(fsiParamsHandle.chanNumber)
            retStatus['Problem'] = 'No_Init_Val'
            returnValues = np.append(returnValues, -1)

    for peakIter in range(0, len(fsiParamsHandle.currentDistance)):

        maxDistance = fsiParamsHandle.currentDistance[peakIter] + fsiParamsHandle.distanceWindow      # Maximum distance to target (m) for the current peak
        minDistance = fsiParamsHandle.currentDistance[peakIter] - fsiParamsHandle.distanceWindow      # Minimum distance to target (m) for the current peak
        
        dstCut, powerCut = dataCutout(distanceData, powerData, minDistance, maxDistance)        # Cut out the distance region of interest for fitting
        returnValues = np.append(returnValues, dstCut[np.argmax(powerCut)])
        print('Distance value before FIT',returnValues)
        
        
        
    if(fit == True):   
        
        for peakIter in range(0, len(fsiParamsHandle.currentDistance)):  
        
            fitModel_Lorentzian = LorentzianModel(nan_policy='omit')  
        # Fit type - Lorentzian, don't generate exceptions in case of NaN values
        #fitModel_Lorentzian = GaussianModel(nan_policy='omit')  
            initialGuess = fitModel_Lorentzian.guess(powerData, x=distanceData)   # Initial guess of the fitted values for the Lorentzian distribution (doesn't work on composite models...)
            initialGuess['amplitude'].min   = 0.0
            initialGuess['sigma'].min       = 0.0
            initialGuess['center'].min      = minDistance
            initialGuess['center'].max      = maxDistance

            try:
                fitOut  = fitModel_Lorentzian.fit(powerCut, initialGuess, x=dstCut)   # Start fitting, fo debug use arg: "iter_cb=fittingDebug" with the fittingDebug() funtion enabled 
                returnValues = np.append(returnValues, fitOut.params['center'].value)
                retStatus['Center error'] = np.append(retStatus['Center error'], fitOut.params['center'].stderr)
            
                if(fullOutput==True):
                    fitMagnitudeData[peakIter] = fitOut.best_fit
                    fitDistanceData[peakIter] = dstCut

            except Exception:
                returnValues = np.append(returnValues, dstCut[np.argmax(powerCut)]) # If the fitting fails, return the distance corresponding to the highest sample.
                retStatus['Message'] = 'FFT fit failed for CH: ' + str(fsiParamsHandle.chanNumber) + '/ peak no: ' 
                str(peakIter) + traceback.format_exc()
                retStatus['Problem'] = 'Fft_Fit_Fail'
                FsiExceptionCounter.fftFittingFailedExcept += 1
                logging.exception('FFT peak fitting error')
        
      
            print(fitOut.fit_report(min_correl=0.25))                               # Show report
            plt.semilogy(dstCut, fitOut.best_fit, 'b-', label='data')               # Plot fit results
            plt.semilogy(dstCut, powerCut, 'g-', label='data')
            plt.show()
      
   
            plt.semilogy(distanceData, powerData)
            plt.xlabel('Distance (m)')
            plt.ylabel('Signal (a.u.)')
            plt.show()
   

            plt.semilogy(distanceData, powerData)
            plt.xlabel('Distance (m)')
            plt.ylabel('Signal (a.u.)')
            plt.show()
     
    
    return fitDistanceData, fitMagnitudeData, returnValues, retStatus
   
