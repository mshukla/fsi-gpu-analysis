
1] Linearization.py
   - Implementation of Butter Filter, Hilbert Transform, ArcTan Wrap
   - Linear Filter to  be applied to Measured and Gas Cell


2] GasCellfit.py
   - Algorithm devised by David to fit Gas Cell plot with standard SRMCSV plot to find the sweeping speed

3] MeasuredCellFFT.py
   - FFT for Linearized Measured Cell Data
   - Fitting the plot using Lorenztian FIT 
   - Returns final Distance 

4]fsiEnums.py, fsiExceptions.py, fsiInitConfiguartions.py
   - FSI instrument related functions,class, and variables provided by Jarek.

 
