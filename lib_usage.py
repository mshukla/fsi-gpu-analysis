#!/usr/bin/env python
# coding: utf-8

from pyfsi_gpu.Linearization import DataLinearize, Interpolate 
from pyfsi_gpu.GasCellfit import gascell_alpha, detect_peaks_gpu
import csv
import numpy as np
import cupy as cp
import time
from pyfsi_gpu.fsiEnums import * 
from pyfsi_gpu.fsiExceptions import *
import pyfsi_gpu.fsiInitConfiguration as initCfg
from pyfsi_gpu.MeasuredCellFFT import *

ref,meas,gas = [],[],[]

with open("data/putty-0923-1.csv", "r") as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for lines in csv_reader:
        ref.append(int(lines[0],16))
        meas.append(int(lines[1],16))
        gas.append(int(lines[2],16))
        
ref=np.array(ref)
meas=np.array(meas)
gas=np.array(gas)

externalData4  = 1 * 1e-08            #sampling period
rawData = {
    'Measurement int'   : meas, # Raw data from the measurement interferometer
    'Gas cell'          : gas, # Raw data from the gas cell
    'Reference int'     : ref, # Raw data from the reference interferometer
    'Time interval'     : externalData4  # Sampling period
}

class FsiSignalParams:
    def __init__(self, CHAN_NUM = 0, CURRENT_DISTANCE = 0.5, DISTANCE_WINDOW = 0.01, PICO_V_RANGE = PICO_RANGE.R200MV, ERROR_COUNTER = 0, FBG = False):
        self.distanceWindow    = DISTANCE_WINDOW    # Distance around center value for the fitting window 
        self.currentDistance   = CURRENT_DISTANCE   # Current value of distance measurement
        self.chanNumber        = CHAN_NUM           # Channel number
        self.picoRange         = PICO_V_RANGE       # Current voltage range of the PicoScope channel
        self.errorCounter      = ERROR_COUNTER      # Error counter
        self.fbg               = FBG                # Defines whether the channel is a Fiber Bragg Grating sensor of an FSI sensor (Boolean)


fsiParamsHandle = FsiSignalParams(2, [0.100000], 0.001, PICO_RANGE.R200MV, 0, False)

class GlobalConfig:
    setupOpMode         = SETUP_OP_MODE.NORMAL              # Setup operation mode
    resultsFileName     = 'Test.csv'                        # Name and path to the file where the measurement results are saved
    externalRawData     = 'Debug\\TestLabViewSecond.mat'    # Name and path to a file containing raw data for analysis (TEST_MODE only!)
    laserParams         = initCfg.LaserInitParams           # Laser configuration
    picoParams          = initCfg.PicoScopeInitParams       # PisoScope configuration
    edfaParams          = initCfg.EdfaInitParams            # EDFA configuration
    foSwitchParams      = initCfg.FoSwitchInitParams        # Fiber optic switch configuration
    photoModuleParams   = initCfg.PhotoModuleInitParams     # Photodetection module configuration
    computationConfig   = initCfg.ComputationConfig         # Configuration of the computation algorithm

    
retStatus ={}
processedDataStorage ={}
zpf= 4 
cLight = 299792458 
nAir = 1.467
time_all = time.time()
start=time.time()
t,t_simu = DataLinearize(rawData['Time interval'],rawData['Reference int'])
FilteredGasCell = Interpolate(t,t_simu, rawData['Gas cell'])
Meas_Linear = Interpolate(t,t_simu, rawData['Measurement int'])
gpu_filter = time.time()-start
print('Time to Filter %s mseconds' %(gpu_filter*1000))

start=time.time()
alpha = gascell_alpha(FilteredGasCell, 'SRM2519a.csv', rawData['Time interval'])
gpu_peak = time.time()-start
print('Time to find peak and sweep %s mseconds' %(gpu_peak*1000))
print('Sweep rate %s THz' %(alpha/ 10**12))


start_time = time.time()
Meas_Linear=cp.array(Meas_Linear)

processedDataStorage['FFT magnitude axis'] = abs(cp.fft.rfft(Meas_Linear, int(len(Meas_Linear)+ (zpf-1)*len(Meas_Linear))))        # "Real" FFT, length includes padded zeros
f = cp.fft.rfftfreq( int(len(Meas_Linear) + (zpf-1)*len(Meas_Linear)), d=rawData['Time interval'])        # Frequency axis calculation, length includes padded zeros
print("--- Time for FFT GPU %s mseconds ---" % ((time.time() - start_time)*1000))
Meas_coef = cLight/(2*abs(alpha)*nAir) # Frequency-to-distance calculation coefficient
print('alpha', abs(alpha))
processedDataStorage['FFT distance axis'] = Meas_coef*f 
start_time= time.time()
processedDataStorage['FFT distance axis'] = cp.asnumpy(processedDataStorage['FFT distance axis'])
processedDataStorage['FFT magnitude axis'] = cp.asnumpy(processedDataStorage['FFT magnitude axis'])
processedDataStorage['FFT fit peaks distance'], processedDataStorage['FFT fit peaks magnitude'], processedDataStorage['Fitted distance values'], retStatus['Fft fit'] = fitFsiPeaks(processedDataStorage['FFT distance axis'], processedDataStorage['FFT magnitude axis'], fsiParamsHandle, GlobalConfig.setupOpMode, fullOutput=True)
print("--- Time for Fit on CPU %s mseconds ---" % ((time.time() - start_time)*1000))
print("--- Time for 1 dataset GPU %s mseconds ---" % ((time.time() - time_all)*1000))
print('Meas FFT Distancess', processedDataStorage['FFT magnitude axis'])
print('Meas FFT Frequency', processedDataStorage['FFT distance axis'])
print(processedDataStorage['Fitted distance values'])


