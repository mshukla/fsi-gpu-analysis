RES_DIR=results
PYTHON_INTERP=python3.7		# Mamta's default
FILE_PATH='.'

[ ! -d "$RES_DIR" ] && mkdir -p "$RES_DIR"

while getopts i:p:t:r:g:m:s:h flag
do
    case "${flag}" in
        p) FILE_PATH=${OPTARG};;
        t) TIMESTAMP=${OPTARG};;
        r) REF_CH=${OPTARG};;
        g) GAS_CH=${OPTARG};;
	m) MEAS_CH=${OPTARG};;
	s) SHOW_PLOT=${OPTARG};; 
	i) PYTHON_INTERP=${OPTARG};;
        \?) echo Usage: $0 -p {File Path} -t {timestamp} -r {Reference Channel} -g {Gas Channel} -m {Meas Channel} -s {show plots};
           echo -h Prints this help message;
           exit 1;;
	h) echo Usage: $0 -p {File Path} -t {timestamp} -r {Reference Channel} -g {Gas Channel} -m {Meas  Channel} -s {show plots};
	   exit 1;;
    esac
done


#Linearize

CUDA_VISIBLE_DEVICE=0, ${PYTHON_INTERP} Linearize-process.py $FILE_PATH $TIMESTAMP $REF_CH $GAS_CH $MEAS_CH $SHOW_PLOT
CUDA_VISIBLE_DEVICE=0, ${PYTHON_INTERP} Meas_FFT-process.py $FILE_PATH $TIMESTAMP $REF_CH $GAS_CH $MEAS_CH t.npy t_simu.npy alpha.npy $SHOW_PLOT
