from pyfsi_gpu.Linearization import DataLinearize, Interpolate 
from pyfsi_gpu.GasCellfit import gascell_alpha, detect_peaks_gpu
from pyfsi_gpu.fsiEnums import * 
from pyfsi_gpu.fsiExceptions import *
import pyfsi_gpu.fsiInitConfiguration as initCfg
from pyfsi_gpu.MeasuredCellFFT import *
from pyfsi_gpu.read_data import *
from pyfsi_gpu.check_data import *
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook 
import csv
import numpy as np
import cupy as cp
import time
import matplotlib.pyplot as plt
import sys
meas = []

ref,gas= [],[]

path=sys.argv[1]
time_req=sys.argv[2]
ref_id=sys.argv[3]
gas_id=sys.argv[4]
meas_id=sys.argv[5]

t      = cp.load("results/"+ str(sys.argv[6]), mmap_mode=None, allow_pickle=True)
t_simu = cp.load("results/"+ str(sys.argv[7]), mmap_mode=None, allow_pickle=True)
alpha  = cp.load("results/"+ str(sys.argv[8]), mmap_mode=None, allow_pickle=True)
show_plot = True if sys.argv[9] in ['y', 'yes', '1', 'True'] else False

retStatus ={}
processedDataStorage ={}
zpf= 4 
cLight = 299792458 
nAir = 1.467
time_sample=1* 1e-8
ref_path, gas_path, meas_path=channel_type(ref_id,gas_id,meas_id,path,time_req)
meas = read_meas(meas_path)
meas = cp.array(meas)

time_all = time.time()

rawData = {
    'Measurement int'   :meas,
    'Time interval'     : time_sample # Sampling period
}

class FsiSignalParams:
    def __init__(self, CHAN_NUM = 0, CURRENT_DISTANCE = 0.5, DISTANCE_WINDOW = 0.001, PICO_V_RANGE = PICO_RANGE.R200MV, ERROR_COUNTER = 0, FBG = False):
        self.distanceWindow    = DISTANCE_WINDOW    # Distance around center value for the fitting window 
        self.currentDistance   = CURRENT_DISTANCE   # Current value of distance measurement
        self.chanNumber        = CHAN_NUM           # Channel number
        self.picoRange         = PICO_V_RANGE       # Current voltage range of the PicoScope channel
        self.errorCounter      = ERROR_COUNTER      # Error counter
        self.fbg               = FBG                # Defines whether the channel is a Fiber Bragg Grating sensor of an FSI sensor (Boolean)


fsiParamsHandle = FsiSignalParams(2, [0.101564], 0.0001, PICO_RANGE.R200MV, 0, True)

class GlobalConfig:
    setupOpMode         = SETUP_OP_MODE.NORMAL              # Setup operation mode
    resultsFileName     = 'Test.csv'                        # Name and path to the file where the measurement results are saved
    externalRawData     = 'Debug\\TestLabViewSecond.mat'    # Name and path to a file containing raw data for analysis (TEST_MODE only!)
    laserParams         = initCfg.LaserInitParams           # Laser configuration
    picoParams          = initCfg.PicoScopeInitParams       # PisoScope configuration
    edfaParams          = initCfg.EdfaInitParams            # EDFA configuration
    foSwitchParams      = initCfg.FoSwitchInitParams        # Fiber optic switch configuration
    photoModuleParams   = initCfg.PhotoModuleInitParams     # Photodetection module configuration
    computationConfig   = initCfg.ComputationConfig         # Configuration of the computation algorithm


Meas_Linear = Interpolate(t,t_simu, rawData['Measurement int'])
start_time = time.time()
Meas_Linear=cp.array(Meas_Linear)

processedDataStorage['FFT magnitude axis'] = abs(cp.fft.rfft(Meas_Linear, int(len(Meas_Linear)+ (zpf-1)*len(Meas_Linear))))        # "Real" FFT, length includes padded zeros
f = cp.fft.rfftfreq( int(len(Meas_Linear) + (zpf-1)*len(Meas_Linear)), d=rawData['Time interval'])        # Frequency axis calculation, length includes padded zeros
print("--- Time for FFT GPU %s mseconds ---" % ((time.time() - start_time)*1000))
Meas_coef = cLight/(2*abs(alpha)*nAir) # Frequency-to-distance calculation coefficient
processedDataStorage['FFT distance axis'] = Meas_coef*f 
start_time= time.time()
processedDataStorage['FFT distance axis'] = cp.asnumpy(processedDataStorage['FFT distance axis'])
processedDataStorage['FFT magnitude axis'] = cp.asnumpy(processedDataStorage['FFT magnitude axis'])
processedDataStorage['FFT fit peaks distance'], processedDataStorage['FFT fit peaks magnitude'], processedDataStorage['Fitted distance values'], retStatus['Fft fit'] = fitFsiPeaks(processedDataStorage['FFT distance axis'], processedDataStorage['FFT magnitude axis'], fsiParamsHandle, GlobalConfig.setupOpMode, fullOutput=True, fit=False)
print("--- Time for 1 dataset GPU %s mseconds ---" % ((time.time() - time_all)*1000))
print('Final Distance Value obtained %s' % processedDataStorage['Fitted distance values'])


if (show_plot):
    plt.plot(processedDataStorage['FFT magnitude axis'])
    plt.suptitle('Meas Channel FFT')
    plt.show()

    plt.loglog(processedDataStorage['FFT magnitude axis'])
    plt.suptitle('Meas Channel log')
    plt.show()




