from pyfsi_gpu.Linearization import DataLinearize, Interpolate 
from pyfsi_gpu.GasCellfit import gascell_alpha, detect_peaks_gpu
import csv
import numpy as np
import cupy as cp
import time
import matplotlib.pyplot as plt
from pyfsi_gpu.read_data import *
import sys
from pyfsi_gpu.check_data import channel_type


ref,gas= [],[]

path      = sys.argv[1]
time_req  = sys.argv[2]
ref_id    = sys.argv[3]
gas_id    = sys.argv[4]
meas_id   = sys.argv[5]
show_plot = True if sys.argv[6] in ['True', '1', 'y', 'yes'] else False
ref_path, gas_path, meas_path = channel_type(ref_id,gas_id,meas_id,path,time_req)

ref = read_ref(ref_path)
gas = read_gas(gas_path)

time_sample=1* 1e-8

rawData = {
    'Gas cell'          : gas, # Raw data from the gas cell
    'Reference int'     : ref, # Raw data from the reference interferometer
    'Time interval'     : time_sample # Sampling period
}

start= time.time()
'''
plt.plot(rawData['Reference int'])
plt.suptitle('Raw Reference Cell Data')
plt.show()

plt.plot(rawData['Gas cell'])
plt.suptitle('Raw Gas Cell Data')
plt.show()
'''
t,t_simu =  DataLinearize(time_sample, rawData['Reference int']) 
print("Time to Filter Ref Cell %s seconds" %(time.time()-start))
#t =cp.asnumpy(t)
#t_simu =cp.asnumpy(t_simu)

cp.save('results/t.npy',t, allow_pickle=True)
cp.save('results/t_simu.npy',t_simu, allow_pickle=True)

FilteredGasCell = Interpolate(t,t_simu, rawData['Gas cell'])
start=time.time()
alpha = gascell_alpha(FilteredGasCell, 'SRM2519a.csv', rawData['Time interval'])
gpu_peak = time.time()-start
print('Time to find peak and sweep %s mseconds' %(gpu_peak*1000))
print('Sweep rate %s THz' %(alpha/ 10**12))
cp.save('results/alpha.npy',alpha, allow_pickle=True)


if (show_plot):
    plt.plot(rawData['Reference int'])
    plt.suptitle('Raw Reference Cell Data')
    plt.show()

    plt.plot(rawData['Gas cell'])
    plt.suptitle('Raw Gas Cell Data')
    plt.show()
    
    FilteredGasCell = cp.asnumpy(FilteredGasCell)
    plt.plot(FilteredGasCell)
    plt.suptitle('Linearized Gas Cell')
    plt.show()


del rawData



